## Tài liệu tham khảo
- [JavaScript SDK reference](https://developer.paypal.com/sdk/js/reference/#buttons) - tài liệu tích hợp nút thanh toán paypal
- [Paypal app](https://viblo.asia/p/ruby-on-rails-tao-button-checkout-paypal-don-gian-Qbq5QQ1X5D8) - Ruby on rails - Tạo button checkout paypal đơn giản
## Yêu cầu
bạn phải cài ruby, rails, postgres

## Chuẩn bị
**1. Tạo tài khoản paypal**
Bạn đăng ký 1 account tại https://www.paypal.com/, 
sau đó có thể dùng để đăng nhập vào trang dành cho developer https://developer.paypal.com

**2. Tạo app**
Tại đây bạn cần tạo 1 app (vd tên là app-test) mới tại https://developer.paypal.com/developer/applications, tạo xong mặc định app sẽ ở chế độ sandbox.

**3. Tạo account**
Ở đây bạn có sẵn 2 account paypal sandbox để test:
- account personal dùng để đăng nhập khi thanh toán
- account business được liên kết với app vừa tạo dùng để nhận tiền
> Note: 2 account sandbox testing bạn có thể đăng nhập vào trang https://www.sandbox.paypal.com/,
đây là trang dùng để test cho trang chủ https://www.paypal.com/

## Cài đặt
Tạo file `.env`
```sh
cd project
cp .env.example .env
```

Chạy dự án
```sh
cd project
bundle
rails db:create
rails db:migrate
rails server
```