class PaymentsController < ApplicationController
  def new
  end
  
  def create
    environment = PayPal::SandboxEnvironment.new(ENV['PAYPAL_CLIENT_ID'], ENV['PAYPAL_SECRET'])
    # environment = PayPal::SandboxEnvironment.new('AeHFJN2Mxov18fzGnkK250reMc7h1oP0vZrnoL6zDL2RJu6gk3hJMdfVqgVbDhrr_h6tgJlb08nV-__W', 'EGTYPz6iRoPcf0hU099tr4cqipb2MRIEl_pSljOLxyusKsbvqWHVTnXPLx7mCBra4CugpOpIAWqwbLIR')
    client = PayPal::PayPalHttpClient.new(environment)
    request = PayPalCheckoutSdk::Orders::OrdersCaptureRequest::new(params[:order_id])
    begin
      # Call API with your client and get a response for your call
      response = client.execute(request)
      # If call returns body in response, you can get the deserialized version from the result attribute of the response
      order = response.result
      Payment.create order_id: order.id, payer_id: order.payer.payer_id
    rescue PayPalHttp::HttpError => ioe
      # Something went wrong server-side
      puts ioe.status_code
      puts ioe.headers["debug_id"]
    end
  end




  # stripe
  def stripe
    # Set your Stripe API key
    Stripe.api_key = ENV['STRIPE_SECRET_KEY']
    
    # Get the token from the request
    token = params[:stripeToken]

    # Create a charge: this will charge the user's card
    begin
      charge = Stripe::Charge.create(
        amount: 1000, # amount in cents
        currency: 'usd',
        source: token,
        description: 'Example Charge'
      )

      flash[:notice] = "Payment successfully stripe"
      redirect_to root_path
    rescue Stripe::CardError => e
      flash[:error] = e.message
      redirect_to root_path
    end
  end


  def stripe_checkout
    Stripe.api_key = ENV['STRIPE_SECRET_KEY']
    session = Stripe::Checkout::Session.create(
      payment_method_types: ['card'],
      line_items: [{
        price_data: {
          currency: 'usd',
          product_data: {
            name: 'T-Shirt',
          },
          unit_amount: 200,
        },
        quantity: 1,
      }],
      mode: 'payment',
      success_url: 'http://localhost:3000/stripe_success?session_id={CHECKOUT_SESSION_ID}',
      cancel_url: 'http://localhost:3000/stripe_cancel?session_id={CHECKOUT_SESSION_ID}',
    )
    redirect_to session.url, status: 303, allow_other_host: true
  end

  def show
    render plain: "thanh toán"
  end

  def stripe_success
    payment_session_id = params[:session_id]
    session = Stripe::Checkout::Session.retrieve(payment_session_id)
    
    render json: {status: session.status, msg: 'Thành Công', session: session}
    # render plain: "thanh toán thành công"
  end

  def stripe_cancel
    payment_session_id = params[:session_id]
    session = Stripe::Checkout::Session.retrieve(payment_session_id)
    
    render json: {status: session.status, msg: 'Cancel', session: session}
    # render plain: "thanh toán cancel"
  end

  # stripe
  def stripe_session_status
    payment_session_id = params[:session_id]
    session = Stripe::Checkout::Session.retrieve(payment_session_id)
    
    render json: {status: session.status, session: session}
  end

end
  