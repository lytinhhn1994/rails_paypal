require_relative "boot"

require "rails/all"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Paypal
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 7.0

    # Configuration for the application, engines, and railties goes here.
    #
    # These settings can be overridden in specific environments using the files
    # in config/environments, which are processed later.
    #
    # config.time_zone = "Central Time (US & Canada)"
    # config.eager_load_paths << Rails.root.join("extras")
  end
end
# AeHFJN2Mxov18fzGnkK250reMc7h1oP0vZrnoL6zDL2RJu6gk3hJMdfVqgVbDhrr_h6tgJlb08nV-__W
# EGTYPz6iRoPcf0hU099tr4cqipb2MRIEl_pSljOLxyusKsbvqWHVTnXPLx7mCBra4CugpOpIAWqwbLIR