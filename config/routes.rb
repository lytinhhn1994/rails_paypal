Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  root "payments#new"
  resources :payments

  post "/payments/stripe" => "payments#stripe", :as => :payments_stripe
  post "/payments/stripe_checkout" => "payments#stripe_checkout", :as => :stripe_checkout

  get "/stripe_success" => "payments#stripe_success", :as => :stripe_success
  get "/stripe_cancel" => "payments#stripe_cancel", :as => :stripe_cancel

  get "/stripe_session_status" => "payments#stripe_session_status", :as => :stripe_session_status

end
